﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HaberOnline.aspx.cs" Inherits="Staj.HaberOnline" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:RadioButtonList ID="rdbL" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
        onselectedindexchanged="rdbL_SelectedIndexChanged" RepeatColumns="14">
    </asp:RadioButtonList>         
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatColumns="12" 
            RepeatDirection="Horizontal" onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
        </asp:RadioButtonList>
    <br />
    <div> 
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="70px" 
                        ImageUrl="../previous button.jpg" Width="251px" OnClick="Imagebutton1_Click_Event" />
                </td>
                <td style="margin-left: 40px" > 
                     <asp:Label id="Label1"  runat="server"></asp:Label>                
                </td>
            </tr>
            <tr><td colspan="2" rowspan="2">&nbsp;<asp:DataList ID="DataList1" runat="server" RepeatColumns="5">
                         <ItemStyle BackColor="#E3E3E1" BorderStyle="Solid" BorderWidth="1px" 
                             Width="250px" HorizontalAlign="Left" VerticalAlign="Top" />

                        <ItemTemplate >
                                 <table cellpadding="0" cellspacing="0">
                                      <tr>
                                         <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px">
                                         <br />
                                 <asp:HyperLink ID="HyperLink1" runat="server" 
                                  NavigateUrl='<%# Eval("Link") %>' Target="_blank" Text='<%# Eval("Title") %>'></asp:HyperLink>
                                   
                                     </td>
                                     </tr>
                                     <tr>
                                     <td  style="font-family: Arial, Helvetica, sans-serif; font-size: 10px">
                                       <br />
                                         <%# Eval("Description")%> 
                                     </td>
                                     </tr>
                                  </table>
                        </ItemTemplate>
                    </asp:DataList></td></tr>  
        </table>
    </div>
    </div>
    </form>
</body>
</html>
