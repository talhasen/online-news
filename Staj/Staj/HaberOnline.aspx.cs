﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Rss;
using System.Xml;

namespace Staj
{
    public partial class HaberOnline : System.Web.UI.Page
    {
        string rssUrl, logoImgUrl, anaSayfaUrl, name, tarih;
        bool control = new bool();
        protected void Page_Load(object sender, EventArgs e)
        {
            ImageButton1.Visible = false;
            try
            {
                if (!IsPostBack)
                {
                    XmlDocument xmlVerisi = new XmlDocument();
                    xmlVerisi.Load(Server.MapPath("~/Dosya/HaberKaynak.xml"));
                    for (int i = 0; i < xmlVerisi.SelectNodes("HaberKaynak/Item/name").Count; i++)
                    {
                        rssUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/rssUrl")[i].InnerText;
                        logoImgUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/logoImgUrl")[i].InnerText;
                        anaSayfaUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/anaSayfaUrl")[i].InnerText;
                        name = xmlVerisi.SelectNodes("HaberKaynak/Item/name")[i].InnerText;
                        rdbL.Items.Add(new ListItem(name, i.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                //hata yazdır
            }
        }
        
        public string findIframe (string name)
        {
                string valueOfLabel=string.Empty;
                switch (name)
                {
                    case "TRT Haber": valueOfLabel = "<div style='font: normal 11px Arial; width: 698px; border: solid 1px #6e95d6; background: #374e91; border-radius: 3px; box-shadow: 1px 1px 3px #6e95d6;'><h2 style='padding: 8px; margin: 0; height: 25px; border: 0; border-bottom: solid 1px #6e95d6;'><a href='http://www.trthaber.com/'><img src='http://s2.trthaber.com/static/images/siteneEkle/logo_k_v2.png' alt='TRT Haber Haberler' style='border: none 0;'></a></h2><iframe frameborder='0' width='698' height='232' src='http://www.trthaber.com/sitene-ekle/gundem-1/?haberSay=10&renk=k&baslik=1&resimler=1'></iframe><div style='text-align: center;line-height: 23px;border-top: solid 1px #6e95d6; color: #ccc; margin-top: 5px;box-shadow: 0 -1px 3px #6e95d6;'><a style='text-decoration: none;color: #ccc; font: normal 11px Arial;' href='http://www.trthaber.com/'>En Son Haberler</a> &nbsp;|&nbsp; <a style='text-decoration: none;color: #ccc; font: normal 11px Arial;' href='http://www.trthaber.com/haber/gundem/'>Gündem Haberleri</a></div></div>"; break;
                    case "Haber7": valueOfLabel = "<iframe src='http://sondakika.haber7.com/manset-slider-500-fade.html' frameborder='0' width='500' height='286' framespacing='0' marginheight='0' marginwidth='0' scrolling='no'></iframe></br><iframe src='http://sondakika.haber7.com/manset-468-60.html' frameborder='0' width='468' height='60' framespacing='0' marginheight='0' marginwidth='0' scrolling='no'></iframe>"; break;
                    case "Samanyolu Haber": valueOfLabel = "<iframe height='400px' width='660px;' draggable='false' src='http://www.samanyoluhaber.com/webmaster/manset-1/index.php'  frameborder=0 ></iframe>"; break;
                    case "Haber Türk": valueOfLabel = "<div style='margin:10px 0 0 10px;'><iframe scrolling='no' horizontalalign='Right' frameborder='0' width='531' height='441' src='http://www.haberturk.com/siteneekle/haberManset'></iframe></div>"; break;
                    case "Yeni Akit": valueOfLabel = "<iframe src='http://www.yeniakit.com.tr/service/?t=0' height='286' width='500' frameborder='0' scrolling='yes'></iframe>"; break;
                    case "TRT Türk": valueOfLabel = "<iframe src='http://www.trtturk.com/siteneekle/slider2' height='320' style='overflow-y: hidden;' frameborder='0' width='100%' align='left'></iframe>"; break;
                    default: valueOfLabel = ""; break;
                }
                return valueOfLabel;
        }

        protected void Imagebutton1_Click_Event(object sender, ImageClickEventArgs e)
        {
            rdbL.Visible = true;
            RadioButtonList1.Visible = false;
            DataList1.Visible = false;
            Label1.Visible = false;
            ImageButton1.Visible = false;
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ImageButton1.Visible = true;
            short i = Convert.ToInt16(RadioButtonList1.SelectedIndex);
            short sira = Convert.ToInt16(rdbL.SelectedIndex);
            XmlDocument xmlVerisi = new XmlDocument();
            xmlVerisi.Load(Server.MapPath("~/Dosya/HaberKaynak.xml"));
            rssUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/topicsUrl" + Convert.ToString(sira))[i].InnerText;
            logoImgUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/logoImgUrl")[sira].InnerText;
            anaSayfaUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/anaSayfaUrl")[sira].InnerText;
            name = xmlVerisi.SelectNodes("HaberKaynak/Item/name")[sira].InnerText;
            RssFeed feed = new RssFeed();// <- bu ve string rssUrl=http://www.trthaber.com/sondakika.rss;
            for (int x = 0; x < 12; x++)
            {
                try
                {
                    feed = RssFeed.Read(rssUrl);
                    if (feed.Channels.Count != 0) break;
                }
                catch
                {
                    XmlReader(rssUrl, logoImgUrl, anaSayfaUrl, name);
                }
            }

            if (feed.Channels.Count == 0)
            {
                DataList1.DataSource = null;
                DataList1.DataBind();
                return;
            }
            
            for (int j = 0; j < feed.Channels.Count; j++)
            {
                RssChannel channel = (RssChannel)feed.Channels[j];
                tarih = channel.LastBuildDate != DateTime.MinValue ? channel.LastBuildDate.ToString() : string.Empty;
                DataList1.Caption = "<a href='" + channel.Link + "' target='_blank'> <img width='200px' alt='" + channel.Description + "' src='" + logoImgUrl + "' /> <br/>" + tarih + " </a>";
                int contentCount = channel.Items.Count;
                for (i = 0; i < channel.Items.Count; i++)
                {
                    if (channel.Items[i].ImageLink.OriginalString != "gopher://rss-net.sf.net")
                    {
                        channel.Items[i].Description = "<img style='border: navy 1px solid;' src='" + channel.Items[i].ImageLink.ToString() + "' border='0' align='left' width='100' height='80'> <br/>" + channel.Items[i].Description;
                    }
                    else
                    {
                        break;
                    }
                }
                DataList1.DataSource = channel.Items;
                DataList1.DataBind();
            }
        }
        
        protected void rdbL_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] topicName;
            XmlDocument xmlVerisi = new XmlDocument();
            xmlVerisi.Load(Server.MapPath("~/Dosya/HaberKaynak.xml"));
            int i = Convert.ToInt32(rdbL.SelectedValue);
            rssUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/rssUrl")[i].InnerText;
            logoImgUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/logoImgUrl")[i].InnerText;
            anaSayfaUrl = xmlVerisi.SelectNodes("HaberKaynak/Item/anaSayfaUrl")[i].InnerText;
            name = xmlVerisi.SelectNodes("HaberKaynak/Item/name")[i].InnerText;
            Label1.Text = findIframe(name);
            if (i != -1)
            {
                rdbL.Visible = false;
                RadioButtonList1.Visible = true;
                DataList1.Visible = true;
                Label1.Visible = true;
                ImageButton1.Visible = true;
            }
            short topicCount = (short)xmlVerisi.SelectNodes("HaberKaynak/Item/topics" + Convert.ToString(i)).Count;
            topicName = new string[topicCount];
            RadioButtonList1.Items.Clear();
            for (short k = 0; k < topicCount; k++)
            {
                topicName[k] = xmlVerisi.SelectNodes("HaberKaynak/Item/topics" + Convert.ToString(i))[k].InnerText;
                RadioButtonList1.Items.Add(topicName[k]);
            }
            RssFeed feed = new RssFeed();
            for (int x = 0; x < 15; x++)
            {
                try
                {
                    control = false;
                    feed = RssFeed.Read(rssUrl);
                    if (feed.Channels.Count != 0) break;
                }
                catch
                {
                    control = true;
                }
            }
            if (control == true)
            {
                DataList1.Visible = false;
                XmlReader(rssUrl, logoImgUrl, anaSayfaUrl, name);
            }
            else
            {
                DataList1.Visible = true;
                if (feed.Channels.Count == 0)
                {
                    DataList1.DataSource = null;
                    DataList1.DataBind();
                    return;
                }

                for (int j = 0; j < feed.Channels.Count; j++)
                {
                    RssChannel channel = (RssChannel)feed.Channels[j];

                    tarih = channel.LastBuildDate != DateTime.MinValue ? channel.LastBuildDate.ToString() : string.Empty;
                    DataList1.Caption = "<a href='" + channel.Link + "' target='_blank'> <img width='200px' alt='" + channel.Description + "' src='" + logoImgUrl + "' /> <br/>" + tarih + " </a>";

                    int contentCount = channel.Items.Count;
                    for (i = 0; i < channel.Items.Count; i++)
                    {
                        if (channel.Items[i].ImageLink.OriginalString != "gopher://rss-net.sf.net")
                        {
                            channel.Items[i].Description = "<img style='border: navy 1px solid;' src='" + channel.Items[i].ImageLink.ToString() + "' border='0' align='left' width='100' height='80'> <br/>" + channel.Items[i].Description;
                        }
                        else
                        {
                            break;
                        }
                    }
                    DataList1.RepeatColumns = name == "Anadolu Ajans" ? 2 : name == "Yeni Akit" ? 2 : 5;
                    DataList1.DataSource = channel.Items;
                    DataList1.DataBind();
                }
            }
        }

        protected void XmlReader(string xmlRss, string xmlLogo, string xmlAnasayfa, string xmlName)
        {
            string tarih = string.Empty,endOfLink;
            XmlDocument xmlVerisi = new XmlDocument();
            xmlVerisi.Load(xmlRss);
            List<kaynak> kList = new List<kaynak>();
            kaynak k;
            for (int i = 0; i < xmlVerisi.SelectNodes("rss/channel").Count; i++)
            {
                try
                {
                    if (true != xmlVerisi.SelectNodes("rss/channel/lastBuildDate").Equals(DateTime.MinValue))
                    {
                        tarih = xmlVerisi.SelectNodes("rss/channel/lastBuildDate")[i].InnerText;
                    }
                }
                catch (Exception ex)
                { 
                }
                DataList1.Caption = "<a href='" + xmlVerisi.SelectNodes("rss/channel/link")[i].InnerText + "' target='_blank'> <img width='200px' alt='" + xmlVerisi.SelectNodes("rss/channel/description")[i].InnerText + "' src='" + xmlLogo + "' /> <br/>" + tarih + "</a>";
                for (int j = 0; j < xmlVerisi.SelectNodes("rss/channel/item").Count; j++)
                {
                    k = new kaynak();
                    endOfLink = string.Empty;
                    k.Title = xmlVerisi.SelectNodes("rss/channel/item/title")[j].InnerText;
                    if (xmlVerisi.SelectNodes("rss/channel/item/link")[j].InnerText != "gopher://rss-net.sf.net")
                    {
                        endOfLink = "</a>";
                        k.Link = "<a href='" + xmlVerisi.SelectNodes("rss/channel/item/link")[j].InnerText + "' target='_blank'> ";
                        k.Description = k.Link + "<img style='border: navy 1px solid;' src='" + xmlVerisi.SelectNodes("rss/channel/item/link")[j].InnerText + "' border='0' align='left' width='200' height='160'> <br/>" + xmlVerisi.SelectNodes("rss/channel/item/description")[j].InnerText + endOfLink;
                    }
                    k.Description = xmlVerisi.SelectNodes("rss/channel/item/description")[j].InnerText;
                    kList.Add(k);
                }
                DataList1.Visible = true;
                DataList1.DataSource = kList;
                DataList1.DataBind();
            }
        }

        public class kaynak
        {
            public string Link { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
        }
    }
}